# -*-coding:utf-8 -*-

# csv操作

import csv

def write(file:'csv文件名',data:'数据',header:'表头'=None):
    '输出csv文件'
    with open(file,'w') as f:
        if header is None:
            # 不带表头写入
            writer=csv.writer(f)
            writer.writerows(data)
        else:
            # 带head
            writer=csv.DictWriter(f,header)
            writer.writeheader()
            writer.writerows(data)
def read(file:'csv文件名',header:'表头'=None)->list:
    '读入csv文件'
    with open(file,'r') as f:
        if header is None:
            # 不带表头读入
            reader=csv.reader(f)
            return list(reader)
        else:
            # 带head
            reader=csv.DictReader(f,header)
            data=[]
            for row in reader:
                data.append(dict(row))
            return data