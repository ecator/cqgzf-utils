# -*-coding:utf-8 -*-

import requests
from lib.getUserAgent import getUserAgent
import math
class BaseInfo:
    def __init__(self,url:'初始化地址',tableName:'请求表名'):
        self.__url=url
        self.__tableName=tableName
        self.__s=requests.Session()
        self.__UserAgent=getUserAgent()
        # 初始化会话
        headers = {
            'User-Agent': self.__UserAgent,
            'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8',
            'Accept-Language': 'zh-CN,zh;q=0.8,zh-TW;q=0.7,zh-HK;q=0.5,en-US;q=0.3,en;q=0.2',
            'Connection': 'keep-alive',
            'Upgrade-Insecure-Requests': '1',
        }
        self.__s.get(self.__url,headers=headers,timeout=10)
        # 获取总页数
        r=self.getPage(1)
        self.totalPage=int(r['totalPage'])
        # 获取总数据量
        r=self.getPage(self.totalPage)
        self.totalItem=int(len(r['dataList'])) + 20 * (self.totalPage-1)
        # 初始化数据元组
        self.__items=[None,]*self.totalItem
    def item(self,i:'数据索引,从1开始')->dict:
        '获取某一条数据,返回字典类型'
        if i<0 or i>self.totalItem:return None
        if self.__items[i-1] is None:
            # 获取数据
            page=math.ceil(i/20)
            r=self.getPage(page)
            for it in range(0,len(r['dataList'])):
                self.__items[20*(page-1)+it]=dict(sorted(r['dataList'][it].items(),key=lambda x:x[0]))
        return self.__items[i-1]
    def getPage(self, page):
        "获取某一页数据,返回字典类型"
        if (self.__url.find('applicationresultdetail')>1):
            # 审核结果公示
            url='http://gzf.zfcxjw.cq.gov.cn:9090/site/cqgzf/queryresultpublic/getSqshjgAction'
        elif (self.__url.find('lotteryresultdetail')>1):
            # 摇号结果公示
            url='http://gzf.zfcxjw.cq.gov.cn:9090/site/cqgzf/lotteryresultpublic/getYhpzjgAction'
        elif (self.__url.find('extendinfopublicity')>1):
            # 续租公示
            url='http://gzf.zfcxjw.cq.gov.cn:9090/site/cqgzf/extendrentpublic/getXzjgAction'
        elif (self.__url.find('exithouseresource')>1):
            # 退出房源公示
            url='http://gzf.zfcxjw.cq.gov.cn:9090/site/cqgzf/extendrentpublic/getTcfyAction'
        else:
            raise Exception('未支持请求')
        headers = {
            'User-Agent': self.__UserAgent,
            'Accept': 'application/json, text/javascript, */*; q=0.01',
            'Accept-Language': 'zh-CN,zh;q=0.8,zh-TW;q=0.7,zh-HK;q=0.5,en-US;q=0.3,en;q=0.2',
            'Referer': self.__url,
            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
            'X-Requested-With': 'XMLHttpRequest',
            'Connection': 'keep-alive',
        }
        data = {
            'isInit': 'true',
            'prefix': 'mobile/',
            'pageNumber': page,
            'xm': '',
            'cnumber': '',
            'sqpq': '',
            'bname': '',
            'code': '',
            'tableName': self.__tableName
        }
        r=self.__s.post(url, headers=headers, data=data,timeout=10)
        return r.json()