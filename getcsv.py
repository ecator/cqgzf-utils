#!/usr/bin/env python3

'获取csv文件,需要传入 cfg.ini 文件中的配置节名称'

from lib.BaseInfo import BaseInfo
from lib import csv
import os,time,logging,sys,configparser
logging.basicConfig(format='%(asctime)s %(levelname)s %(message)s',level=logging.INFO)
if (len(sys.argv)==1):
    logging.error('请传入配置节名称 useage => %s section_name'%(__file__))
    exit(1)
else:
    sec=sys.argv[1]
try:
    cfg=configparser.ConfigParser()
    cfg.read('cfg.ini')
    desription=cfg.get(sec,'description')
    url=cfg.get(sec,'url')
    tableName=cfg.get(sec,'tableName')
except Exception as e:
    logging.error(e)
    exit(1)
else:
    logging.info(desription)
    logging.info('url:%s'%(url))
    logging.info('tableName:%s'%(tableName))

csvf=desription+'_'+time.strftime('%Y%m%d%H%M%S',time.localtime())+'.csv'
try:
    p=BaseInfo(url,tableName)
except Exception as e:
    logging.error('初始化失败 %s'%(e))
    exit(1)
data=[]
success=0
retry=False
while (success!=p.totalItem) :
    data=[]
    success=0
    for i in range(1,p.totalItem+1):
        try:
            data.append(p.item(i))
        except Exception as e:
            logging.info('item%06d 失败 %s'%(i,e))
            logging.info('延迟10秒。。。')
            time.sleep(10)
        else:
            logging.info('item%06d 成功'%(i))
            success+=1
            if (success%1000 == 0 and retry==False):
                logging.info('延迟10秒。。。')
                time.sleep(10)
    if (success!=p.totalItem):
        logging.warning('未全部获取成功，开始重试')
        retry=True

logging.info('全部获取成功 : %d'%(p.totalItem))
try:
    csv.write(csvf,data=data,header=p.item(1).keys())
except Exception as e:
    logging.info('输出文件"%s"失败 %s'%(csvf,e))
else:
    logging.info('输出文件"%s"成功'%(csvf))