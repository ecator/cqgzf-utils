# 重庆公租房工具

重庆公租房数据工具，主要用于抓取数据以及简单的分析，目前以支持的四种数据来说明。


# 审核结果公示
## csv字段

1. `appcode`:申请编号
2. `cnumber`:身份证号
3. `appperson`:姓名
4. `cstate`:状态
5. `xmmc`:申请片区
6. `gdate`:申请时间
7. `sqhx`:申请户型
8. `sex`:性别
9. `dept`:工作单位
10. `rysr`:个人收入
11. `fincome`:家庭收入
12. `sqfs`:申请方式
13. `isHouse`:有无住房
14. `sqmj`:申请面积

# 摇号结果公示
## csv字段

1. `sqbh`:申请编号
2. `cnumber`:身份证号
3. `bcode`:未知(None固定)
4. `xqmc`:申请片区
5. `zh`:住房地址
6. `fh`:房号
7. `hx`:户型
8. `rtel`:未知(None固定)
9. `xm`:姓名
10. `sex`:性别
11. `dept`:工作单位
12. `sqfs`:申请方式

# 续租公示
## csv字段
1. `code`:申请编号
2. `appperson`:姓名
3. `cnumber`:身份证号
4. `bname`:项目名称
5. `sqhs`:申请户型
6. `czsj`:未知(None固定)
7. `fwdz`:房屋地址
8. `zt`:状态

# 退出房源公示
## csv字段
1. `id`:未知
2. `houseInfo`:退出房源信息
3. `pcId`:未知